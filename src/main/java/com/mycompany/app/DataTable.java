package com.mycompany.app;

public class DataTable {

    private final String srId;
    private final String phoneNumber;
    private final String phoneNumberCompared;
    private final int levenshteinDistance;
    private final String srIdCompared;
    private final double processingTime;

    public String getSrId() {
        return srId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneNumberCompared() {
        return phoneNumberCompared;
    }

    public int getLevenshteinDistance() {
        return levenshteinDistance;
    }

    public String getSrIdCompared() {
        return srIdCompared;
    }

    public double getProcessingTime() {
        return processingTime;
    }

    public DataTable(String srId, String phoneNumber, String phoneNumberCompared, int levenshteinDistance, String srIdCompared, double processingTime) {
        this.srId = srId;
        this.phoneNumber = phoneNumber;
        this.phoneNumberCompared = phoneNumberCompared;
        this.levenshteinDistance = levenshteinDistance;
        this.srIdCompared = srIdCompared;
        this.processingTime = processingTime;
    }

    public String printHeader() {
        return "\tSR_ID + \t\tPHONE_NUMBER + \t\t\tPHONE_NUMBER_COMPARED + \t\t\tLD + \t\tSR_ID_COMPARED";
    }

    @Override
    public String toString() {
        return "\t" + srId + "\t\t" + phoneNumber + "\t\t\t" + phoneNumberCompared + "\t\t\t" + levenshteinDistance + "\t\t" + srIdCompared + "\t\t" + processingTime;
    }
}

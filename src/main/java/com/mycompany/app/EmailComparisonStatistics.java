package com.mycompany.app;

public class EmailComparisonStatistics {

    private long distance0 = 0;
    private long distance1 = 0;
    private long distance2 = 0;
    private long distance3 = 0;
    private long distance4 = 0;
    private long distance5 = 0;
    private long distance6 = 0;
    private long distance7 = 0;
    private long distance8 = 0;
    private long distance9 = 0;
    private long distance10 = 0;

    public long getDistance0() {
        return distance0;
    }

    public void setDistance0(long distance0) {
        this.distance0 = distance0;
    }

    public long getDistance1() {
        return distance1;
    }

    public void setDistance1(long distance1) {
        this.distance1 = distance1;
    }

    public long getDistance2() {
        return distance2;
    }

    public void setDistance2(long distance2) {
        this.distance2 = distance2;
    }

    public long getDistance3() {
        return distance3;
    }

    public void setDistance3(long distance3) {
        this.distance3 = distance3;
    }

    public long getDistance4() {
        return distance4;
    }

    public long getDistance5() {
        return distance5;
    }

    public void setDistance5(long distance5) {
        this.distance5 = distance5;
    }

    public long getDistance6() {
        return distance6;
    }

    public void setDistance6(long distance6) {
        this.distance6 = distance6;
    }

    public long getDistance7() {
        return distance7;
    }

    public void setDistance7(long distance7) {
        this.distance7 = distance7;
    }

    public long getDistance8() {
        return distance8;
    }

    public void setDistance8(long distance8) {
        this.distance8 = distance8;
    }

    public long getDistance9() {
        return distance9;
    }

    public void setDistance9(long distance9) {
        this.distance9 = distance9;
    }

    public long getDistance10() {
        return distance10;
    }

    public void setDistance10(long distance10) {
        this.distance10 = distance10;
    }

    public void setDistance4(long distance4) {
        this.distance4 = distance4;
    }

    public void printCount(long total) {
        System.out.println("LD Distance 0  : " + distance0 + " " + (double) distance0 * 100 / total + "%");
        System.out.println("LD Distance 1  : " + distance1 + " " + (double) distance1 * 100 / total + "%");
        System.out.println("LD Distance 2  : " + distance2 + " " + (double) distance2 * 100 / total + "%");
        System.out.println("LD Distance 3  : " + distance3 + " " + (double) distance3 * 100 / total + "%");
        System.out.println("LD Distance 4  : " + distance4 + " " + (double) distance4 * 100 / total + "%");
        System.out.println("LD Distance 5  : " + distance5 + " " + (double) distance5 * 100 / total + "%");
        System.out.println("LD Distance 6  : " + distance6 + " " + (double) distance6 * 100 / total + "%");
        System.out.println("LD Distance 7  : " + distance7 + " " + (double) distance7 * 100 / total + "%");
        System.out.println("LD Distance 8  : " + distance8 + " " + (double) distance8 * 100 / total + "%");
        System.out.println("LD Distance 9  : " + distance9 + " " + (double) distance9 * 100 / total + "%");
        System.out.println("LD Distance 10 : " + distance10 + " " + (double) distance10 * 100 / total + "%");
    }
}

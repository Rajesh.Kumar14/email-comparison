package com.mycompany.app;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class EmailComparisonPOC {

    public static final int EMAIL = 2;
    public static final int ROW_COUNT = 5471;
    public static final int SR_ID = 0;
    public static final String FILE_NAME = "src/main/resources/Spam_Fraud_SRs.xlsx";
    public static final String SHEET_NAME = "5k";

    public static void main(String args[]) throws IOException {

        XSSFSheet sheet = openSheet();

        Map<String, String> allRequests = new HashMap<>(ROW_COUNT);
        List<DataTable> tableList = new ArrayList<>();
        Date startTime = new Date();
        for (Row row : sheet) {
            long reqStartTime = System.nanoTime();
            Cell cell = row.getCell(EMAIL);
            if (cell != null) {
                String email = extractEmail(cell);

                if (!email.equals("EMAIL") && !(email.isEmpty())) {
                    String srId = String.valueOf(BigDecimal.valueOf(row.getCell(SR_ID).getNumericCellValue()).toBigInteger());
                    tableList.add(getDataTable(allRequests, email, srId, reqStartTime));
                    allRequests.put(srId, email);
                }
            }
            if (row.getRowNum() > ROW_COUNT) break;
        }

        System.out.println("Total Time Taken = " + (new Date().getTime() - startTime.getTime()) / 1000D + " seconds");
        printStatistics(tableList);

    }

    private static String extractEmail(Cell cell) {
        return cell.getStringCellValue();
    }

    private static XSSFSheet openSheet() throws IOException {
        XSSFSheet sheet = null;
        try (OPCPackage opcPackage = OPCPackage.open(new File(FILE_NAME))) {
            sheet = new XSSFWorkbook(opcPackage).getSheet(SHEET_NAME);
        } catch (InvalidFormatException e) {
            System.out.println("Error while open the excel sheet: " + e.getMessage());
        }
        return sheet;
    }

    private static void printStatistics(List<DataTable> tableList) {
        tableList.forEach(System.out::println);
        long size = tableList.size();
        System.out.println("\n\nTotal requests: " + size);
        EmailComparisonStatistics lsd = new EmailComparisonStatistics();
        for (DataTable dataTable : tableList) {
            switch (dataTable.getLevenshteinDistance()) {
                case 0 -> lsd.setDistance0(lsd.getDistance0() + 1);
                case 1 -> lsd.setDistance1(lsd.getDistance1() + 1);
                case 2 -> lsd.setDistance2(lsd.getDistance2() + 1);
                case 3 -> lsd.setDistance3(lsd.getDistance3() + 1);
                case 4 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 5 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 6 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 7 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 8 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 9 -> lsd.setDistance4(lsd.getDistance4() + 1);
                case 10 -> lsd.setDistance4(lsd.getDistance4() + 1);
            }
        }
        lsd.printCount(size);
    }

    private static DataTable getDataTable(Map<String, String> allRequests, String inputEmail, String srId, double startTime) {
        int lowestDistance = 99;
        String srIdCompared = "NO_SR";
        String emailCompared = "NO_EMAIL";
        for (Map.Entry<String, String> entry : allRequests.entrySet()) {
            String email = entry.getValue();
            if (email.equals(inputEmail)) {
                lowestDistance = 0;
                srIdCompared = entry.getKey();
                emailCompared = email;
                break;
            } else {
                int emailIndex = email.indexOf("@");
                int inputEmailIndex = inputEmail.indexOf("@");
                if ((email.substring(0, emailIndex).startsWith(inputEmail.substring(0, inputEmailIndex))
                    || inputEmail.substring(0, inputEmailIndex).startsWith(email.substring(0, emailIndex))) && (email.substring(emailIndex).equals(inputEmail.substring(inputEmailIndex)))) {
                    int distance = Math.abs(email.substring(0, emailIndex).length() - inputEmail.substring(0, inputEmailIndex).length());
                    if (distance < lowestDistance) {
                        lowestDistance = distance;
                        srIdCompared = entry.getKey();
                        emailCompared = email;
                    }
                }
            }
        }
        return new DataTable(srId, inputEmail, emailCompared, lowestDistance, srIdCompared, (System.nanoTime() - startTime) / 1000000D);
    }
}
